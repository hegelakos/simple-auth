import { OpenAPIV3 } from 'openapi-types';
import { statusEndpointFactory } from './endpoints/status.endpoint';
import { Endpoint } from './framework/endpoint/endpoint';
import { fastifyServerFactory } from './framework/fastify/fastify-server.factory';
import { fastifySwaggerFactory } from './framework/fastify/fastify-swagger-factory';
import { ConfigObject } from './config';
import { Logger } from './logging';
import { loginEndpointFactory } from './endpoints/login.endpoint';
import { FastifyInstance } from 'fastify';
import { statusAuthenticatedEndpointFactory } from './endpoints/status-authenticated.endpoint';

export const createApp = ({ config, logger }: { config: ConfigObject; logger: Logger }): FastifyInstance => {
  // const tokenService = jwtServiceFactory({
  //   secret: config.jwt.secret,
  //   expiresIn: config.jwt.expiresIn,
  // });

  // const authenticationService = authenticationServiceFactory({
  //   tokenService,
  // });

  const statusEndpoint = statusEndpointFactory();
  const loginEndpoint = loginEndpointFactory({
    expiresIn: config.jwt.expiresIn,
    secret: config.jwt.secret,
    auth: config.auth,
    logger,
  });
  const statusAuthenticatedEndpoint = statusAuthenticatedEndpointFactory({
    secret: config.jwt.secret,
    auth: config.auth,
    logger: logger,
  });

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const endpoints = [statusEndpoint, loginEndpoint, statusAuthenticatedEndpoint];

  const securityRequirementObjects: OpenAPIV3.SecurityRequirementObject[] = [
    {
      bearerToken: [],
    },
  ];

  const components: OpenAPIV3.ComponentsObject = {
    schemas: {},
    securitySchemes: {
      bearerToken: {
        type: 'http',
        scheme: 'bearer',
      },
    },
  };

  const tags = [
    {
      name: 'status',
      description: 'Status related endpoints.',
    },
    {
      name: 'user',
      description: 'User related endpoints.',
    },
  ];

  const swaggerOptions = fastifySwaggerFactory({
    tags,
    components,
    security: securityRequirementObjects,
    host: config.swagger.domain,
  });

  const server = fastifyServerFactory({
    swaggerOptions,
    logger,
    endpoints: endpoints as Endpoint[],
  });

  return server;
};
