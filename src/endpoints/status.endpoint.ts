import { Endpoint, EndpointMethod } from '../framework/endpoint/endpoint';

export const statusEndpointFactory = (): Endpoint => ({
  method: EndpointMethod.GET,
  route: '/status',
  schema: {
    tags: ['status'],
    description: 'This endpoint is responsible for retrieving the status of the API.',
  },
  handler: () => {
    return Promise.resolve({
      status: 200,
      response: { status: 'ok' },
    });
  },
});
