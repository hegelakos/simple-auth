import * as jsonwebtoken from 'jsonwebtoken';
import { Endpoint, EndpointMethod } from '../framework/endpoint/endpoint';
import { APIError } from '../framework/errors/errors';
import { getTokenFromHeader } from '../framework/http/get-token';
import { Logger } from '../logging';

export const statusAuthenticatedEndpointFactory = ({
  secret,
  auth,
  logger,
}: {
  secret: string;
  auth: { mail: string; password: string };
  logger: Logger;
}): Endpoint<{}, {}, {}, {}, {}> => ({
  method: EndpointMethod.GET,
  route: '/status/authenticated',
  schema: {
    tags: ['status'],
    description: 'This status endpoint is only available if your token is valid.',
  },
  handler: (request) => {
    const token = getTokenFromHeader(request.headers);
    try {
      const decoded = jsonwebtoken.verify(token, secret) as {
        mail: string;
        password: string;
        iat: number;
        exp: number;
      };
      if (decoded.mail === auth.mail && decoded.password === auth.password) {
        logger.info('user authenticated');
        return Promise.resolve({
          status: 200,
          response: { status: 'ok' },
        });
      }
      logger.info('invalid user');
      throw new APIError(403, 'invalid user');
    } catch (error) {
      if (error.name) {
        if (error.name === 'TokenExpiredError' || error.name === 'JsonWebTokenError') {
          throw new APIError(403, error.message);
        }
      }
      throw error;
    }
  },
});
