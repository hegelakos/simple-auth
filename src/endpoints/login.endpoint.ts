import { Endpoint, EndpointMethod } from '../framework/endpoint/endpoint';
import * as jsonwebtoken from 'jsonwebtoken';
import { APIError } from '../framework/errors/errors';
import { Logger } from '../logging';

type Body = {
  mail: string;
  password: string;
};
export const loginEndpointFactory = ({
  secret,
  expiresIn,
  auth,
  logger,
}: {
  secret: string;
  expiresIn: string;
  auth: { mail: string; password: string };
  logger: Logger;
}): Endpoint<{}, {}, {}, Body, {}> => ({
  method: EndpointMethod.POST,
  route: '/login',
  schema: {
    tags: ['user'],
    description: 'Login endpoint.',
    body: {
      type: 'object',
      required: ['mail', 'password'],
      additionalProperties: false,
      properties: {
        mail: { type: 'string' },
        password: { type: 'string' },
      },
    },
  },
  handler: (request) => {
    const { mail, password } = request.body;
    if (mail === auth.mail && password === auth.password) {
      const token = jsonwebtoken.sign({ mail, password }, secret, { expiresIn });
      logger.info('successfully logged in');
      return Promise.resolve({ status: 200, response: { token } });
    }
    throw new APIError(403, 'unauthorized');
  },
});
