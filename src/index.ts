import { createApp } from './app';
import { config } from './config';
import { loggerFactory } from './logging';

const thrower = (err: unknown): void => {
  throw err;
};

const throwToGlobal = (err: unknown): NodeJS.Immediate => setImmediate(() => thrower(err));

process.on('unhandledRejection', throwToGlobal);

const startApp = async (): Promise<void> => {
  const logger = loggerFactory({ name: 'simple-auth-server logger', level: config.logger.level, version: '1.0.0' });
  const app = createApp({ config, logger });
  await app.listen(config.port);
  await app.oas();
  logger.info(`server started on port ${config.port}`);
};

startApp().catch(throwToGlobal);
