import convict from 'convict';
import dotenv from 'dotenv';

export type ConfigObject = {
  swagger: {
    domain: string;
  };
  port: number;
  auth: {
    mail: string;
    password: string;
  };
  jwt: {
    secret: string;
    expiresIn: string;
  };
  logger: {
    level: string;
  };
};

dotenv.config();

export const configObject = convict<ConfigObject>({
  swagger: {
    domain: {
      format: String,
      default: 'localhost:3000',
      env: 'APP_DOMAIN',
    },
  },
  port: {
    format: Number,
    default: 3000,
    env: 'PORT',
  },
  auth: {
    mail: {
      format: String,
      default: 'test@codingsans.com',
      env: 'AUTH_MAIL',
    },
    password: {
      format: String,
      default: 'P4S$W03D',
      env: 'AUTH_PASSWORD',
    },
  },
  jwt: {
    secret: {
      format: String,
      default: 'very-very-secret-string',
      env: 'JWT_SECRET',
    },
    expiresIn: {
      format: String,
      default: '1h',
      env: 'JWT_EXPIRES_IN',
    },
  },
  logger: {
    level: {
      format: String,
      default: 'info',
      env: 'LOGGER_LEVEL',
    },
  },
});

configObject.validate({ allowed: 'strict' });

export const config = configObject.getProperties();
