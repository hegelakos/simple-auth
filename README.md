# simple-auth

## Local steps

1. Install dependencies by `yarn install`
2. Build the sources `yarn build`
3. Start the backend by `yarn start`
4. Check the documentation (http://localhost:3000/documentation)

## Environment variables

The easiest way to change the default variables is to create a .env file and define your variables there. All the env variables have default values.

default mail: `test@codingsans.com`
default password: `P4S$W03D`

Find more at `src/config.ts`

## Endpoint

You should find 3 endpoints registrated to the application.

1. `GET - status endpoint`
2. `GET - authenticated status endpoint`
3. `POST - login endpoint`

Find more in the swagger documentation (http://localhost:3000/documentation)
